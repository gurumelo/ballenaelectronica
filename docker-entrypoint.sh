#!/bin/bash
service rsyslog start
service mysql start
service cron start
service bind9 start
echo "nameserver 127.0.0.1" > /etc/resolv.conf
service redis-server start
service rspamd start
postmap /etc/postfix/without_ptr
newaliases
service postfix start
service dovecot start

if [ ! -f /contenedor ]; then
    mysql -uroot -ppassword_maria < /root/bd.sql
    certbot certonly --rsa-key-size 4096 --standalone -d nombre_host --non-interactive --agree-tos --email postmaster@nombre_host
    touch /contenedor

    echo -e "\n\033[0;31mIMPORTANTE/IMPORTANT:\033[0m"
    echo -e "Añade este registro TXT a tu DNS/Add this TXT record to your DNS"
    echo -e "\033[0;36mSUBDOMINIO/SUBDOMAIN:\033[0m"
    echo -e "2022._domainkey.nombre_host"
    echo -e "\033[1;32mCONTENIDO/CONTENT:\033[0m"
    sed -e "s|CPU doesn't have SSSE3 instructions set required for hyperscan, disable it||g" -e 's|2022._domainkey IN TXT ( "||g' -e 's| ) ;||g' -e 's|"||g' -ze 's|\t||g' -ze 's|\n||g'  /var/lib/rspamd/dkim/2022.txt 
    echo -e "\n"
fi

/bin/bash
