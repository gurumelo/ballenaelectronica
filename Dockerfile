# Imagen basada en debian estable
FROM debian:stretch-slim

# Tu dominio / Your domain
ARG nombre_host=cripto.pantallasnegras.net
# Tu IPv4 / Your IPv4
ARG ip_version4=1.1.1.1
# Tu IPv6 / Your IPv6
ARG ip_version6=1:1:1:1
# Imagina una contraseña para MariaDB / Imagine password for MariaDB
ARG password_maria=probando
# Imagina un nombre para la base de datos / Imagine database name
ARG varnombrebd=vmaila
# Imagina un usuario para la base de datos / Imagine database username
ARG varusuaribd=vmailb
# Imagina una contraseña para el usuario de la base de datos / Imagine password for database username
ARG varbdcontra=vmailpassc
# Imagina una contraseña para interfaz web de Rspamd / Imagine password for Rspamd web interface
ARG contrasena_rspamd=1234

RUN echo $nombre_host > /etc/mailname
ARG DEBIAN_FRONTEND=noninteractive
ARG APT_KEY_DONT_WARN_ON_DANGEROUS_USAGE=DontWarn 
RUN apt-get update
RUN apt-get install --yes apt-utils
RUN apt-get install --yes apt-transport-https
RUN echo "deb https://debian.inf.tu-dresden.de/debian stretch main contrib" > /etc/apt/sources.list
RUN echo "deb https://debian.inf.tu-dresden.de/debian-security stretch/updates main contrib" >> /etc/apt/sources.list
RUN echo "deb https://debian.inf.tu-dresden.de/debian stretch-updates main contrib" >> /etc/apt/sources.list
RUN apt-get update

#Rsyslog
RUN apt-get install --yes rsyslog --no-install-recommends

#MariaDB
RUN echo mariadb-server mysql-server/root_password password $password_maria | debconf-set-selections
RUN echo mariadb-server mysql-server/root_password_again password $password_maria | debconf-set-selections
RUN apt-get install --yes mariadb-server

RUN adduser --disabled-login --disabled-password --home /var/vmail --gecos '' vmail
RUN mkdir /var/vmail/mailboxes
RUN mkdir -p /var/vmail/sieve/global
RUN chown -R vmail /var/vmail
RUN chgrp -R vmail /var/vmail
RUN chmod -R 770 /var/vmail

#Dovecot
RUN apt-get install --yes dovecot-core dovecot-imapd dovecot-lmtpd dovecot-mysql dovecot-sieve dovecot-managesieved
RUN rm -r /etc/dovecot/*
COPY archivos/dovecot.conf /etc/dovecot/ 
RUN sed -i "s|nombre_host|$nombre_host|g" /etc/dovecot/dovecot.conf
COPY archivos/dovecot-sql.conf /etc/dovecot/
RUN sed -i -e "s|varnombrebd|$varnombrebd|g" -e "s|varusuaribd|$varusuaribd|g" -e "s|varbdcontra|$varbdcontra|g" /etc/dovecot/dovecot-sql.conf
RUN chmod 440 /etc/dovecot/dovecot-sql.conf 
COPY archivos/spam-global.sieve /var/vmail/sieve/global/
COPY archivos/learn-spam.sieve /var/vmail/sieve/global/
COPY archivos/learn-ham.sieve /var/vmail/sieve/global/

#Postfix
RUN apt-get install --yes postfix postfix-mysql
RUN rm -r /etc/postfix/sasl
RUN rm /etc/postfix/master.cf /etc/postfix/main.cf.proto /etc/postfix/master.cf.proto
COPY archivos/main.cf /etc/postfix/
RUN sed -i -e "s|ip_version4|$ip_version4|g" -e "s|ip_version6|$ip_version6|g" -e "s|nombre_host|$nombre_host|g" /etc/postfix/main.cf
COPY archivos/master.cf /etc/postfix/
COPY archivos/submission_header_cleanup /etc/postfix/
RUN mkdir /etc/postfix/sql
COPY archivos/accounts.cf /etc/postfix/sql/
RUN sed -i -e "s|varnombrebd|$varnombrebd|g" -e "s|varusuaribd|$varusuaribd|g" -e "s|varbdcontra|$varbdcontra|g" /etc/postfix/sql/accounts.cf
COPY archivos/aliases.cf /etc/postfix/sql
RUN sed -i -e "s|varnombrebd|$varnombrebd|g" -e "s|varusuaribd|$varusuaribd|g" -e "s|varbdcontra|$varbdcontra|g" /etc/postfix/sql/aliases.cf
COPY archivos/domains.cf /etc/postfix/sql
RUN sed -i -e "s|varnombrebd|$varnombrebd|g" -e "s|varusuaribd|$varusuaribd|g" -e "s|varbdcontra|$varbdcontra|g" /etc/postfix/sql/domains.cf
COPY archivos/recipient-access.cf /etc/postfix/sql
RUN sed -i -e "s|varnombrebd|$varnombrebd|g" -e "s|varusuaribd|$varusuaribd|g" -e "s|varbdcontra|$varbdcontra|g" /etc/postfix/sql/recipient-access.cf
COPY archivos/sender-login-maps.cf /etc/postfix/sql
RUN sed -i -e "s|varnombrebd|$varnombrebd|g" -e "s|varusuaribd|$varusuaribd|g" -e "s|varbdcontra|$varbdcontra|g" /etc/postfix/sql/sender-login-maps.cf
COPY archivos/tls-policy.cf /etc/postfix/sql
RUN sed -i -e "s|varnombrebd|$varnombrebd|g" -e "s|varusuaribd|$varusuaribd|g" -e "s|varbdcontra|$varbdcontra|g" /etc/postfix/sql/tls-policy.cf
RUN chmod -R 640 /etc/postfix/sql
RUN touch /etc/postfix/without_ptr
RUN touch /etc/postfix/postscreen_access

#Rspamd
RUN apt-get install --yes lsb-release wget gnupg bind9 redis-server --no-install-recommends
RUN wget -O- https://rspamd.com/apt-stable/gpg.key | apt-key add - > /dev/null
RUN echo "deb http://rspamd.com/apt-stable/ $(lsb_release -c -s) main" > /etc/apt/sources.list.d/rspamd.list
RUN echo "deb-src http://rspamd.com/apt-stable/ $(lsb_release -c -s) main" >> /etc/apt/sources.list.d/rspamd.list
RUN apt-get update
RUN apt-get install --yes rspamd
COPY archivos/options.inc /etc/rspamd/local.d/
COPY archivos/worker-normal.inc /etc/rspamd/local.d/
RUN echo 'password = "'$(rspamadm pw -p ${contrasena_rspamd})'";' | sed "s|CPU doesn't have SSSE3 instructions set required for hyperscan, disable it ||g" > /etc/rspamd/local.d/worker-controller.inc
COPY archivos/worker-proxy.inc /etc/rspamd/local.d/
COPY archivos/logging.inc /etc/rspamd/local.d/
COPY archivos/milter_headers.conf /etc/rspamd/local.d/
COPY archivos/classifier-bayes.conf /etc/rspamd/local.d/
COPY archivos/redis.conf /etc/rspamd/local.d/

#DKIM
RUN mkdir /var/lib/rspamd/dkim/
RUN rspamadm dkim_keygen -b 1024 -s 2022 -k /var/lib/rspamd/dkim/2022.key > /var/lib/rspamd/dkim/2022.txt
RUN chown -R _rspamd:_rspamd /var/lib/rspamd/dkim
RUN chmod 440 /var/lib/rspamd/dkim/*

#Let's encrypt
RUN apt-get install --yes cron --no-install-recommends
RUN apt-get install --yes certbot --no-install-recommends
RUN rm /etc/cron.d/certbot
COPY archivos/certbot /etc/cron.d/

#BD
COPY archivos/bd.sql /root 
RUN sed -i -e "s|varnombrebd|$varnombrebd|g" -e "s|varusuaribd|$varusuaribd|g" -e "s|varbdcontra|$varbdcontra|g" /root/bd.sql

#scripts
COPY archivos/scripts/ballena-dominiocrear.sh /usr/local/bin
RUN sed -i "s|varnombrebd|$varnombrebd|g" /usr/local/bin/ballena-dominiocrear.sh
RUN chmod +x /usr/local/bin/ballena-dominiocrear.sh
COPY archivos/scripts/ballena-correocrear.sh /usr/local/bin
RUN sed -i "s|varnombrebd|$varnombrebd|g" /usr/local/bin/ballena-correocrear.sh
RUN chmod +x /usr/local/bin/ballena-correocrear.sh
COPY archivos/scripts/ballena-aliascrear.sh /usr/local/bin
RUN sed -i "s|varnombrebd|$varnombrebd|g" /usr/local/bin/ballena-aliascrear.sh
RUN chmod +x /usr/local/bin/ballena-aliascrear.sh


COPY docker-entrypoint.sh /
RUN sed -i -e "s|password_maria|$password_maria|g" -e "s|nombre_host|$nombre_host|g" /docker-entrypoint.sh
RUN chmod +x /docker-entrypoint.sh
ENTRYPOINT ["/docker-entrypoint.sh"]
