## Crear en su DNS los siguientes registros

```
tudominio.net          A       1.1.1.1
tudominio.net          AAAA    1:1:1:1
imap.tudominio.net     CNAME   tudominio.net
smtp.tudominio.net     CNAME   tudominio.net
tudominio.net          MX      tudominio.net
tudominio.net          TXT     v=spf1 a:mail.mysystems.tld ?all
_dmarc.tudominio.net   TXT     v=DMARC1; p=none
```

## Bajar código

```
git clone https://gitlab.com/gurumelo/ballenaelectronica
```

## Modificar variables en Dockerfile

```
cd ballenaelectronica
nano Dockerfile
```

## Construir imagen

```
docker build -t ballenaelectronica .
```

## Ejecutar contenedor, modificando tudominio.net

```
docker run --name ballena -p 80:80 -p 25:25 -p 143:143 -p 587:587 -h tudominio.net -it ballenaelectronica 
```

## Al finalizar, se le muestra que debe crear un registro DNS, referente a la configuración de DKIM

## Operativa

```
# Crear un dominio 
root@cripto:/# ballena-dominiocrear.sh 

# Crear una dirección de correo electrónico
root@cripto:/# ballena-correocrear.sh  

# Crear un alias de correo
root@cripto:/# ballena-aliascrear.sh 
```

# Configuración Thunderbird

```
usuario: nombre@tudominio.net
IMAP: imap.tudominio.net    143    STARTTLS    Password, normal
SMTP: smtp.tudominio.net    587    STARTTLS    Password, normal
```


*Inspiration*: https://thomas-leister.de/en/mailserver-debian-stretch/